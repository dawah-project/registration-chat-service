FROM markadams/chromium-xvfb-js:latest

RUN apt-get remove -y nodejs
RUN apt-get update
RUN apt-get install -y curl software-properties-common
RUN curl -sL https://deb.nodesource.com/setup_12.x | bash -
RUN apt-get install -y nodejs
RUN apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

COPY package*.json ./
RUN npm ci --only=production
COPY . .
CMD [ "npm", "start" ]