const sulla = require('sulla-hotfix');
const pg = require('pg');
const validator = require('validator');
const connectionString = process.env.DB_URL || 'postgres://hsi:hsi@localhost:5444/hsi';
const db = new pg.Pool({connectionString: connectionString});
const fs = require('fs');

sulla.create().then(client => start(client));

async function start(client) {

    // contacts = await client.getAllContacts();
    // console.log('length', contacts.length);
    // console.log('all contacts', contacts);
    
    const chats = await client.getAllChatsWithMessages(false);
    // console.log("TCL: start -> chats", chats.length);

    jsonContent = JSON.stringify(chats);

    fs.writeFile("chats.json", jsonContent, 'utf8', function (err) {
        
        if (err) {
            console.log("An error occured while writing JSON Object to File.");
            return console.err(err);
        }
     
        console.log("JSON file has been saved.");
    });

    // for(let i = 0; i < chats.length; i++){

    //     chat = chats[i];
    //     isGroup = chat.id.split('-').length > 1;

    //     if(!isGroup){
    //         chat.msgs.map(message => {

    //             command = String(message.body).trim();
    //             isUUID = validator.isUUID(command);

    //             if(message.type === 'chat' && message.from !== '6283872082835@c.us' && isUUID){
                    
    //                 time = new Date(message.t*1000);

    //                 text = 'INSERT INTO chats("from", "message", "date", "to", "is_uuid") VALUES($1, $2, $3, $4, $5) RETURNING *'
    //                 values = [
    //                     message.from,
    //                     message.body,
    //                     `${time.toLocaleDateString()} ${time.toLocaleTimeString()}`,
    //                     message.to,
    //                     isUUID
    //                 ]

    //                 // promise
    //                 db.query(text, values)
    //                     .then(res => {
    //                         console.log('insert', i, res.rows[0]['from'], 'succeed')
    //                     })
    //                     .catch(e => console.error(e))
    //             }
    //         });
    //     }
     
    //  }

    // chats.map(chat => {
    //     const isGroup = chat.id.split('-').length > 1;

    //     if(!isGroup){
    //         chat.msgs.map(message => {

    //             const command = String(message.body).trim();
    //             const isUUID = validator.isUUID(command);

    //             if(message.type === 'chat' && message.from !== '6283872082835@c.us' && isUUID){
                    
    //                 const time = new Date(message.t*1000);

    //                 const text = 'INSERT INTO chats("from", "message", "date", "to", "is_uuid") VALUES($1, $2, $3, $4, $5) RETURNING *'
    //                 const values = [
    //                     message.from,
    //                     message.body,
    //                     `${time.toLocaleDateString()} ${time.toLocaleTimeString()}`,
    //                     message.to,
    //                     isUUID
    //                 ]

    //                 // promise
    //                 db.query(text, values)
    //                     .then(res => {
    //                         console.log('insert', res.rows[0]['from'], 'succeed')
    //                     })
    //                     .catch(e => console.error(e))
    //             }
    //         });
    //     }
    // });
    // console.log("TCL: start ->chats", chats[0].msgs);

    // const newMessages = await client.getAllUnreadMessages();
    // console.log("TCL: start -> newMessages", newMessages)

    // const chats = await client.getAllChats(true);
    // console.log("TCL: start -> chats", chats);
    // chats.map(message => {

    //     if(message.isGroup === false && message.kind === 'chat' && message.contact.isMe === false){
    //         console.log(message);
    //     }
    // });
    // console.log("TCL: getAllNewMessages ->", newMessages.length, newMessages[0]);

    // try {
    //     const chats = await client.getAllChatsWithMessages(true);
    //     console.log("TCL: getAllChatsWithMessages ->", chats);
    //     // console.log("TCL: start ->chats", chats[0].msgs);

    //     const newMessages = await client.getAllNewMessages(true);
    //     console.log("TCL: getAllNewMessages ->", newMessages);

    // } catch (error) {
    //     console.log("TCL: start -> error", error);
    // }

    client.onMessage(async message => {

        try {
            console.log('from -> ', message.from);
            // const chats = await client.getAllChatsWithMessages(true);
            // console.log("TCL: getAllChatsWithMessages ->", chats);
            

            // const chats = await client.getAllChats(true);
            // chats.forEach(async chat => {
            //     if(!chat.isGroup){
            //         console.log('from: ', chat.id.user);
            //         message = await client.getChatById(chat.id. _serialized)
            //         console.log(message.msgs)
            //     }
            // });

        } catch (error) {
            console.log("TCL: start -> error", error);
        }
    });
  }