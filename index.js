const sulla = require('sulla-hotfix');
const axios = require('axios');
const validator = require('validator');
const pg = require('pg');
const connectionString = process.env.DB_URL || 'postgres://hsi:hsi@localhost:5444/hsi';
const adminPhone = process.env.ADMIN_PHONE || '6283872082835';
// const db = new pg.Client(connectionString);
const db = new pg.Pool({connectionString: connectionString});

sulla.create().then(client => start(client));

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

function start(client) {

    client.onMessage(async message => {

        // console.log(message);
        const command = message.body.toLowerCase().trim();
        const changeWA = command.substring(0, 7);
        const number = message.from.split('@')[0]

        if (command === 'bantuan') {
            client.sendText(message.from, 'Panduan penggunaan:\n\nUntuk mengetahui status pendaftaran, kirimkan pesan berupa kode unik yang diberikan pada saat pendaftaran. Pastikan tidak ada tambahan karakter apapun pada pesan yang dikirim.\nUntuk merubah no WhatsApp yang sudah pernah didaftarkan silahkan gunakan aplikasi WhatsApp yang sudah menggunakan nomor yang baru, lalu kirim pesan dengan format "Ubah WA spasi kode unik pendaftaran". Contoh: *Ubah WA 1234-1234-1234-1234*');
        }
        else if (command === 'all' && number === adminPhone) {

            try {
                const chats = await client.getAllChats(true);

                chats.forEach(chat => {
                    
                    const phone = chat.id.user
                    const from = chat.id._serialized

                    if(!chat.isGroup && phone !== adminPhone){

                        sleep(1000).then(() => {
                            
                            try {
                                db.query('SELECT * FROM members WHERE nomer_hp = $1::text', [phone], (err, res) => {
                                    
                                    if (!err && res.rows.length){
                                        
                                        const text = 'UPDATE members SET status=$1 WHERE nomer_hp = $2 RETURNING *'
                                        const values = ['active', phone]
                            
                                        db
                                        .query(text, values)
                                        .then(res => {
                                            const newMember = res.rows[0]
                                            console.log('user', newMember['nama_lengkap'], 'hp ', phone, 'telah aktif')
                                            client.sendText(from, `Selamat ${newMember['nama_lengkap']} anda telah terdaftar sebagai peserta utama HSI angkatan 201.\n\nKode daftar: ${newMember['uuid']} (harap dirahasiakan).\n\nInfo selanjutnya akan disampaikan via aplikasi.\n\n*Mohon untuk tidak menghapus aplikasi HSI dari heandphone anda.*`);
                                        })
                                        .catch(e => {
                                            // db.end()
                                            console.error(e.stack)
                                        })
                                    } else {
                                        console.error('error ', err);
                                        console.error('data', phone,'not exists');
                                        client.sendText(from, 'Mohon maaf, terjadi kesalahan pada sistem, mohon kirim ulang kode unik pendaftaran anda.');
                                    }
                                });
                            } catch (error) {
                                console.log(error);
                            }
                        });
                    }
                });
            } catch (error) {
                console.log("error ->", error);
            }
        }
        else if (changeWA === 'ubah wa') {

            let cmdArr = command.split(' ')
            let lastKey = cmdArr.length - 1;
            let uuid = cmdArr[lastKey]

            if(validator.isUUID(uuid)) {
            
                axios.put('https://api.abdullahroy.id/v1/update-nomor-hp', {
                    "uuid": uuid,
                    "number": number
                })
                .then(function (response) {
                    client.sendText(message.from, 'Nomor WhatsApp anda sudah berhasil diupdate menjadi '+number+'. Silahkan kirim ulang kode unik pendaftaran untuk kembali melakukan aktifasi.');
                })
                .catch(function (error) {

                    if (error.response.status === 404 ) {
                        client.sendText(message.from, 'Data tidak dapat kami temukan. Pastikan anda telah mendaftar dan telah mendapatkan kode unik yang benar.');
                    } else {
                        client.sendText(message.from, 'Mohon maaf, terjadi kesalahan pada sistem.');
                    }
                });
            }
            else {
                client.sendText(message.from, 'Format kode unik yang anda masukan salah. Pastikan anda telah mendaftar dan telah mendapatkan kode unik yang benar.');
            }
        }
        else if (validator.isUUID(command)) {
            // console.log('from:', message.from.split('@')[0]);
            // console.log('to:', message.to.split('@')[0]);
            
            axios.post('https://api.abdullahroy.id/v1/callback-pickyassist', {
                "number": number,
                "message-in": message.body,
                "type": "1",
                "application": "1",
                "unique-id": "5642",
                "project-id": "27"
            })
            .then(function (response) {
                // console.log(response.data['message-out']);

                if (response.status === 200) {
                    client.sendText(message.from, response.data['message-out']);
                }
            })
            .catch(function (error) {
                // console.log(error);

                if (error.response.status === 400 ) {
                    client.sendText(message.from, 'Data tidak dapat kami temukan. Pastikan anda telah mendaftar dan telah mendapatkan kode unik yang benar.');
                } else {
                    client.sendText(message.from, 'Mohon maaf, terjadi kesalahan pada sistem.');
                }
            });
        }
        else {
            client.sendText(message.from, 'Ini adalah pesan penjawab otomatis. 🙏 mohon maaf, pesan anda belum bisa kami proses. Ketik *Bantuan* untuk mendapatkan panduan penggunaan.');
        }
  });
}