const sulla = require('sulla-hotfix');
const axios = require('axios');
var validator = require('validator');

sulla.create().then(client => start(client));

function start(client) {
    client.onMessage(message => {
        client.sendText(message.from, 'Alhamdulillah, terima kasih sudah mendaftar daurah bersama ustadz Dr. Abdullah Roy, MA. Pemberitahuan selanjutnya akan dikirimkan melalui pesan WhatsApp ini, jazakumullah khairan.');
    });
}